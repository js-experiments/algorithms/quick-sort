const observable = factory => (...args) => {
  const observers = [];
  const obj = factory(...args);
  const notify = (event, ...args) =>
    observers
      .filter(o => o.event === event)
      .forEach(o => o.callback.apply(obj, [event, ...args]));

  return Object.assign(
    {
      subscribe(event, callback) {
        observers.push({
          event,
          callback,
        });
      },
      notify,
    },
    obj,
  );
};

const quickSort = observable(() => {
  return {
    list: [],
    add(n) {
      this.list.push(n);
      this.notify('add', n);
    },
    sort(list = this.list) {
      if (list.length <= 1) {
        return list;
      }

      const pivot = list[0];
      const left = [];
      const right = [];
      this.notify('set-pivot', pivot);

      for (let i = 1; i < list.length; i++) {
        const item = list[i];
        if (item <= pivot) {
          left.push(item);
        } else {
          right.push(item);
        }
      }

      this.notify('pick-left', left);
      this.notify('pick-right', right);

      const sortedLeft = this.sort(left);
      const sortedRight = this.sort(right);
      const sorted = [...sortedLeft, pivot, ...sortedRight];

      this.notify('set-sorted', sorted);

      return sorted;
    },
  };
});

const randomArr = size => {
  const list = [];
  let count = 0;
  while (count < size) {
    let rand = Math.floor(Math.random() * size + 1);
    while (list.indexOf(rand) > -1) {
      rand = Math.floor(Math.random() * size + 1);
    }
    list.push(rand);
    count++;
  }

  return list;
};

const getTranslate = (str, axis = 'x') => {
  return str === ''
    ? 0
    : parseFloat(
        str
          .split('translate3d')[1]
          .replace(/, /gi, ',')
          .split(' ')[0]
          .replace(/\(|\)/gi, '')
          .split(',')[axis === 'x' ? 0 : axis === 'y' ? 1 : 2],
      );
};

const renderer = (sorter, toAdd, mainElm = null) => {
  // DOM Elements
  const mainElement = mainElm || document.getElementById('main');
  const speedElement = document.getElementById('speed');
  const algorithmContainer = document.createElement('section');
  const algorithmElement = document.createElement('div');

  let toAnimate = [];
  let intervalId;
  let speed = +speedElement.value;
  let nextAnimation;
  let added = 0;
  let lastY = 0;

  speedElement.addEventListener('change', () => {
    clearInterval(intervalId);
    speed = +speedElement.max + +speedElement.min - +speedElement.value;
    render(nextAnimation);
  });

  const onAdd = (type, value) => {
    toAnimate.push({
      type,
      value: [value],
    });
  };

  const onSetPivot = (type, pivot) => {
    toAnimate.push({
      type,
      value: [pivot],
    });
  };

  const onPickLeft = (type, left) => {
    toAnimate.push({
      type,
      value: left,
    });
  };

  const onPickRight = (type, right) => {
    toAnimate.push({
      type,
      value: right,
    });
  };

  const onSetSorted = (type, sorted) => {
    toAnimate.push({
      type,
      value: sorted,
    });
  };

  const createValueElement = v => {
    const modifiers = [
      '--top',
      '--bottom',
      '--front',
      '--back',
      '--left',
      '--right',
    ];
    const mainElement = document.createElement('div');
    mainElement.classList.add('element', 'box', `value_${v}`);
    const faces = Array.from(Array(6), (item, index) => {
      const face = document.createElement('div');
      face.classList.add('box__face', `box__face${modifiers[index]}`);
      face.textContent = v;
      mainElement.appendChild(face);

      return face;
    });

    return mainElement;
  };

  const animateList = (list, {xIni, yIni, zIni}) => {
    clearInterval(intervalId);
    const animating = [];
    const onElementAnimationEnd = event => {
      animating.pop();
      event.target.removeEventListener('transitionend', onElementAnimationEnd);
      if (animating.length === 0) {
        render();
      }
    };

    list.forEach((element, index) => {
      const x = getTranslate(element.style.transform) || 0;
      const y = getTranslate(element.style.transform, 'y') || 0;
      const z = getTranslate(element.style.transform, 'z') || 0;

      const end = `translate3d(${xIni +
        index * 100}px, ${yIni}px, ${zIni}px) rotateX(0) rotateZ(0)`;

      setTimeout(() => {
        element.style.transition = `transform ${speed}ms ease-out`;
        element.style.transitionDelay = `${speed * index}ms`;
        element.style.transform = end;
        element.addEventListener('transitionend', onElementAnimationEnd);
        animating.push(element);
      }, speed * 0.4);
    });
  };

  const render = function(next) {
    nextAnimation = next;
    intervalId = setTimeout(() => {
      const animation = toAnimate.shift();

      if (!animation) {
        clearInterval(intervalId);
        speedElement.disabled = false;
        if (next) {
          return next.call(this);
        }

        return;
      }

      const {value, type} = animation;
      if (value.length === 0) {
        return render();
      }
      const elements = value.map(
        v => document.querySelector(`.value_${v}`) || createValueElement(v),
      );

      switch (type) {
        case 'add':
          elements.forEach(e => {
            algorithmElement.appendChild(e);
            e.style.transform = `translate3d(${added *
              100}px, 0, 0) rotateX(0) rotateZ(0)`;
          });
          animateList(elements, {xIni: added * 100, yIni: 0, zIni: -1000});
          added++;
          break;
        default:
          let xIni = 0;
          const zIni = getTranslate(elements[0].style.transform, 'z') + 100;
          // [2,5,8,9,7,4,10,6,1,3]

          if (type === 'pick-left') {
            xIni = 500 - elements.length * 100;
          } else if (type === 'pick-right') {
            xIni = 600;
          } else if (type === 'set-pivot') {
            xIni = 500;
            let yIni =
              Math.max(getTranslate(elements[0].style.transform, 'y'), lastY) +
              50;
            lastY = yIni;
          } else {
            xIni = 0;
          }
          animateList(elements, {xIni, yIni: lastY, zIni});
      }
    }, speed);
  };

  sorter.subscribe('add', (e, value) => onAdd(e, value));
  sorter.subscribe('set-pivot', (e, pivot) => onSetPivot(e, pivot));
  sorter.subscribe('pick-left', (e, left) => onPickLeft(e, left));
  sorter.subscribe('pick-right', (e, right) => onPickRight(e, right));
  sorter.subscribe('set-sorted', (e, sorted) => onSetSorted(e, sorted));

  return {
    renderAddSort() {
      lastY = 0;
      toAdd.forEach(n => sorter.add(n));

      sorter.sort();

      return this;
    },

    start(next) {
      algorithmContainer.classList.add('algorithm');
      algorithmElement.classList.add('algorithm__elements');
      algorithmContainer.appendChild(algorithmElement);
      mainElement.appendChild(algorithmContainer);
      render.apply(this, [next]);

      // Clean list in an ugly way ;)
      sorter.list = [];
      speedElement.disabled = true;

      return this;
    },
    stop() {
      toAnimate = [];
      added = 0;
      clearInterval(intervalId);
      algorithmElement.innerHTML = '';
      algorithmElement.remove();
      algorithmContainer.innerHTML = '';
      algorithmContainer.remove();

      return this;
    },
  };
};

const arr = randomArr(10);

const algorithmRenderer = renderer(quickSort(), arr);

const menu = document.getElementById('algorithm-menu');
const menuItems = menu.querySelectorAll('li');
const onClickMenuItem = function(e) {
  const action = this.dataset.renderer;

  algorithmRenderer
    .stop()
    .start()
    [action]();
};

menuItems.forEach(item => item.addEventListener('click', onClickMenuItem));
